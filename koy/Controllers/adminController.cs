﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace koy.Controllers
{
    public class adminController : Controller
    {
        // GET: admin
        public ActionResult Index()
        {

            return View();
        }
        mvcEntities2 db = new mvcEntities2();
        public ActionResult kullanicilar()
        {
            List<users> kullanicilar = db.users.ToList();
            ViewBag.liste = kullanicilar;
            return View();
        }
        public ActionResult makaleler()
        {
            List<makaleler> makale = db.makaleler.ToList();
            ViewBag.liste = makale;
            return View();
        }
       
       
        public FileContentResult download(int id)
        {

           
            byte[] fileData;
            string fileName;
            
            var record = from p in db.makaleler
                         where p.id == id
                         select p;
            
            fileData = (byte[])record.First().File.ToArray();
            fileName = record.First().FileName;
            
            return File(fileData, "text", fileName);

        }
       
        
       
        
    }
}