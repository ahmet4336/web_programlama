﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace koy.Controllers
{
    public class homeController : Controller
    {
        // GET: home
        public ActionResult Index()
        {



            return View();
        }
        public ActionResult register()
        {


            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult register(users model)
        {
            if (ModelState.IsValid)
            {
                using (mvcEntities2 dc = new mvcEntities2())
                {
                    
                    dc.users.Add(model);
                    dc.SaveChanges();
                    ModelState.Clear();
                    model = null;
                    ViewBag.Message = "Successfully Registration Done";
                    
                }
            }

            return View(model);
        }
        public ActionResult login()
        {


            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult login(users model)
        {



            if (ModelState.IsValid)
            {
                using (mvcEntities2 db = new mvcEntities2())
                {



                    var v = db.users.Where(a => a.email.Equals(model.email) && a.sifre.Equals(model.sifre) && a.rolu == null).FirstOrDefault();
                    if (v != null)
                    {
                        Session["LogedUserID"] = v.userid.ToString();
                        Session["LogedUserFullname"] = v.email.ToString();
                        return RedirectToAction("AfterLogin", "Home");
                    }

                    var u = db.users.Where(k => k.email.Equals(model.email) && k.sifre.Equals(model.sifre) && k.rolu.Equals("admin")).FirstOrDefault();

                    if (u.rolu.Equals("admin") && u.rolu != null)
                    {


                        return Redirect("~/admin/Index");

                    }

                }


            }

            return View(model);
        }
        public ActionResult AfterLogin()
        {
            if (Session["LogedUserID"] != null)
            {

                //



                //


                return View();
            }
            else
            {
                return RedirectToAction("Index", "home");
            }
        }


        public ActionResult logoff()
        {

            Session.Clear();
            return RedirectToAction("Index", "home");
        }
        public ActionResult MakaleGonder()
        {

            return View();
        }
        public ActionResult makaleTuru()
        {
            return View();

        }
        [HttpPost]
        public ActionResult makaleTuru([Bind(Include =
"fullBaslik,makaleTuru")] makaleler model2)
        {

            if (ModelState.IsValid)
            {
                using (mvcEntities2 dc = new mvcEntities2())
                {

                    dc.makaleler.Add(model2);
                    dc.SaveChanges();
                   
                    
                    

                }
            }



            return View(model2);

        }

        public ActionResult MakaleGonder(MyViewModel model, FormCollection fc, makaleler model2)
        {

            if (Session["LogedUserID"] != null)
            {
                mvcEntities2 me = new mvcEntities2();
                if (!ModelState.IsValid)
                {
                    return View(model);
                }
                makaleler fileUploadModel = new makaleler();
                byte[] uploadFile = new byte[model.File.InputStream.Length];
                model.File.InputStream.Read(uploadFile, 0, uploadFile.Length);

                fileUploadModel.FileName = model.File.FileName;
                fileUploadModel.File = uploadFile;
                fileUploadModel.email = fc["email"];

                me.makaleler.Add(fileUploadModel);
                me.SaveChanges();

                return Content("File Uploaded.");





            }


            else

                return RedirectToAction("Index", "home");
        }









    }
}